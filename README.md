# Esphome

## MacOS install
```
pip install esphome
```

### Upgrading
```
pip3 install esphome --upgrade
```

## Usage
```
esphome -h

esphome version

esphome run <file>.yml
```